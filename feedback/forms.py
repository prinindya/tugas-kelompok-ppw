from django import forms

#import model dari models.py
from .models import formfdk

class FeedbackForm(forms.ModelForm):
    class Meta:
        model = formfdk
        fields = [
            'namaDepan',
            'namaBelakang',
            'email',
            'feedback',
        ]     

        widgets = {
            'namaDepan' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Tuliskan Nama Depan Anda'}),
            'namaBelakang' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Tuliskan Nama Belakang Anda'}),
            'email' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Tuliskan Email Anda'}),
            'feedback' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Tuliskan Feedback Anda'}),
        }
