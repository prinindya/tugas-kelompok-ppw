from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import FeedbackForm
from .models import formfdk

# Create your views here.
def feedback(request):
    if(request.method=='POST'):
        fdk = FeedbackForm(request.POST or None)
        if fdk.is_valid():
            fdk.save()
            return HttpResponseRedirect("/feedback")
    response = {'form_feedback' : FeedbackForm}
    return render(request, 'feedback.html', response)
    
