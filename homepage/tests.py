from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from homepage.models import *
from homepage.forms import *
from homepage.views import *



# Create your tests here.

class SusunDeadlineTest(TestCase):

    def test_homepageapp_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
    
    def test_susundeadline_is_exist(self):
        response = Client().get('/susundeadline')
        self.assertEqual(response.status_code,200)

    def test_daftarjadwal_is_exist(self):
        response = Client().get('/daftarjadwal')
        self.assertEqual(response.status_code,200)

    def test_models_create(self):
        p1 = PostModel.objects.create(Mata_Kuliah="PPW",Nama_Tugas="TK1")
        hitungJumlah = PostModel.objects.all().count()
        self.assertEqual(hitungJumlah,1)

    def test_validate_model(self):
        test = PostModel.objects.create(Mata_Kuliah="PPW",Nama_Tugas="TK1")
        with self.assertRaises(ValidationError, msg='maafEinsteintidak bisa diposting'):
            validate_author('Einstein')

    def test_model_name(self):
        PostModel.objects.create(Mata_Kuliah="PPW",Nama_Tugas="TK1")
        postModel = PostModel.objects.get(Mata_Kuliah="PPW")
        self.assertEqual(str(postModel),'PostModel object (1)')

