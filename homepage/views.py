from django.shortcuts import render
from django.http import HttpResponseRedirect
# Create your views here.
from .forms import PostForm, QuoteForm
from .models import PostModel, formquote

def homepage(request):
    return render(request, 'homepage.html')

def index(request):
    posts = PostModel.objects.all()
    context = {
        'heading' : 'Post',
        'contents': 'List Post',
        'posts' : posts,
    }
    return render(request, 'daftarjadwal.html', context)

def create(request):
    post_form = PostForm(request.POST or None)
    if request.method == 'POST':
        #cara ambil data dari input user ke database dg validasi
        if post_form.is_valid():
            post_form.save()
        return HttpResponseRedirect("/daftarjadwal")

    
    context = {
        'pageTitle' : 'Create Form',
        'post_form' : post_form,
    }
    return render(request, 'susundeadline.html', context)

def update(request, update_id):
    akun_update = PostModel.objects.get(id=update_id)
    data = {
        'Mata_Kuliah' : akun_update.Mata_Kuliah,
        'Nama_Tugas' : akun_update.Nama_Tugas,
        'Deadline' : akun_update.Deadline,
    }
    post_form = PostForm(request.POST or None, initial=data, instance=akun_update)

    if request.method == 'POST':
        #cara ambil data dari input user ke database dg validasi
        if post_form.is_valid():
            post_form.save()
        return HttpResponseRedirect("/daftarjadwal")

    
    context = {
        'pageTitle' : 'Update Form',
        'post_form' : post_form,
    }
    return render(request, 'story5/susundeadline.html', context)

def delete(request, delete_id):
    PostModel.objects.filter(id=delete_id).delete()
    return HttpResponseRedirect("/daftarjadwal")

def quotes(request):
    if(request.method=='POST'):
        qts = QuoteForm(request.POST or None)
        if qts.is_valid():
            qts.save()
            return HttpResponseRedirect("/quotes")
    response = {'form_quote' : QuoteForm}
    return render(request, 'FormQuotes.html', response)

def cetakquote(request):
    infoquote = formquote.objects.all()
    print(infoquote)
    response = {'quotesnya' : infoquote}
    return render(request, 'quotes.html', response)





    