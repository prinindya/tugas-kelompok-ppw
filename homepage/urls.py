from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('susundeadline', views.create, name='create'),
    path('daftarjadwal', views.index),
    path('quotes', views.cetakquote, name="cetakquote"),
    path('formquotes', views.quotes, name="quotes"),
    path('update/<int:update_id>', views.update, name='update'),
    path('delete/<int:delete_id>', views.delete, name='delete'),
    path('quotes', views.cetakquote, name="cetakquote"),
]