from django import forms

#import model dari models.py
from .models import PostModel, formquote

class PostForm(forms.ModelForm):
    class Meta:
        model = PostModel
        fields = [
            'Mata_Kuliah',
            'Nama_Tugas',
            'Deadline',           
        ]
        widgets = {
            'Mata_Kuliah': forms.TextInput(  
                attrs={
                    'class' : 'form-control', 
                    'placeholder' : 'Tuliskan nama Mata kuliah',
                }
            ),
            'Nama_Tugas': forms.TextInput(  
                attrs={
                    'class' : 'form-control',
                    'placeholder' : 'Tuliskan nama tugas anda',
                }
            ),      
            'Deadline': forms.TextInput(  
                attrs={
                    'class' : 'form-control', 
                    'placeholder' : 'Tuliskan tenggat waktu dari tugas anda',
                }
            ),
        }
# pada bagian ini form susun deadline memiliki tiga data yang harus disubmit oleh pengguna

#lisa:

class QuoteForm(forms.ModelForm):
    class Meta:
        model = formquote
        fields = '__all__'

        widgets = {
            'Quote' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Tuliskan Quote Motivasi'}),
            'nama' : forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Tuliskan Nama'}),
        }

    error_messages = { 'required' : 'Please Type' }

    input_attrs = {
        'type' : 'text',
    }



