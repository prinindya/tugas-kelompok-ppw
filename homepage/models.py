from django.db import models
from django.core.exceptions import ValidationError
# Create your models here.
# memvalidasi di model krn yg ingin di validasi model 

def validate_author(value):
    judul_input = value
    if judul_input == "Einstein":
        message = "maaf" + judul_input + "tidak bisa diposting"
        raise ValidationError(message)

class PostModel(models.Model):
    Mata_Kuliah = models.CharField(max_length=100)
    Nama_Tugas = models.TextField(
        max_length=300,
        validators= [validate_author]
        )
    Deadline = models.CharField(max_length=100)

    published = models.DateTimeField(auto_now_add=True)
    
def __str__(self):
    return "{}.{}".format(self.id, self.judul)

class formquote(models.Model):
    Quote = models.CharField(max_length=500)
    nama = models.CharField(max_length=100)

    def __str__(self):
        return self.Quote

