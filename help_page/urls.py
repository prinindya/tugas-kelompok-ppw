from django.urls import path
from . import views

app_name = 'help_page'

urlpatterns = [
    path('', views.index, name='help-page'),
]