from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Question
from .views import index

# Create your tests here.
class HelpPageUnitTest(TestCase):
    def test_helpPage_url_is_exist(self):
        response=Client().get('/help-page/')
        self.assertEqual(response.status_code,200)

    def test_helpPage_model_created(self):
        Question.objects.create()
        jumlah = Question.objects.all().count()
        self.assertEqual(jumlah,1)

    def test_does_index_views_show_correct_template(self):
        response = Client().get('/help-page/')
        self.assertTemplateUsed(response, 'help_page.html')

    def test_save_a_POST_request(self):
        response = Client().post('/help-page/', data={'pertanyaan': 'This is a question?'})
        self.assertEqual(response.status_code,200)

