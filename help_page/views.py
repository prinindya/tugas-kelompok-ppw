from django.shortcuts import render, redirect
from .models import Question
from .forms import QuestionBox

def index(request):
    my_form = QuestionBox()

    if request.method == "POST":
        my_form = QuestionBox(request.POST)
        if my_form.is_valid():
            Question.objects.create(**my_form.cleaned_data)
            my_form = QuestionBox()
        context = {
            'form' : my_form,
            'boolean' : True
        }
        return render(request, 'help_page.html', context)
    else:
        my_form = QuestionBox()

        context = {
            'form' : my_form,
            'boolean' : False
        }
        return render(request, 'help_page.html', context)

